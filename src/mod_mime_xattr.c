/* $Id: mod_mime_xattr.c 12 2006-01-23 01:17:14Z lennart $ */

/***
   Copyright 2004-2006 Lennart Poettering

   Licensed under the Apache License, Version 2.0 (the "License"); you
   may not use this file except in compliance with the License.  You
   may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
   implied.  See the License for the specific language governing
   permissions and limitations under the License.
***/

#include <httpd.h>
#include <http_config.h>
#include <http_protocol.h>
#include <http_core.h>
#include <http_log.h>
#include <http_main.h>
#include <http_request.h>
#include <apr_lib.h>
#include <ap_config.h>
#include <apr_strings.h>
#include <unixd.h>
#include <apr_signal.h>
#include <mpm_common.h>

#include <sys/types.h>
#include <attr/xattr.h>
#include <ctype.h>

#ifdef __GNUC__
#define __unused __attribute__ ((unused))
#else
#define __unused
#endif


#define XATTR_NAME_MIMETYPE "user.mime_type"
#define XATTR_NAME_MIMETYPE2 "user.mime-type"
#define XATTR_NAME_ENCODING "user.mime_encoding"
#define XATTR_NAME_ENCODING2 "user.mime-encoding"
#define XATTR_NAME_CHARSET "user.charset"
#define XATTR_NAME_HANDLER "user.apache_handler"
#define XATTR_NAME_HANDLER2 "user.apache-handler"

module AP_MODULE_DECLARE_DATA mime_xattr_module;

struct mime_xattr_dir_config {
    int enable_mime_type;
    int enable_mime_type_set;
    int enable_handler;
    int enable_handler_set;
};

static void* create_mime_xattr_dir_config(apr_pool_t *p, __unused char *dummy) {
    struct mime_xattr_dir_config *c = apr_palloc(p, sizeof(struct mime_xattr_dir_config));

    c->enable_mime_type = 0;
    c->enable_mime_type_set = 0;
    c->enable_handler = 0;
    c->enable_handler_set = 0;
    
    return c;
}

static void *merge_mime_xattr_dir_config(apr_pool_t *p, void *basev, void *overridesv) {
    struct mime_xattr_dir_config *base = basev, *override = overridesv, *c;

    c = apr_palloc(p, sizeof(struct mime_xattr_dir_config));

    c->enable_mime_type = override->enable_mime_type_set ? override->enable_mime_type : base->enable_mime_type;
    c->enable_mime_type_set = override->enable_mime_type_set || base->enable_mime_type_set;
    c->enable_handler = override->enable_handler_set ? override->enable_handler : base->enable_handler;
    c->enable_handler_set = override->enable_handler_set || base->enable_handler_set;

    return c;
}

static const char *enable_xattr_mime_type(__unused cmd_parms *parms, void *mconfig, int flag) {
    struct mime_xattr_dir_config *c = mconfig;
    
    c->enable_mime_type = flag;
    c->enable_mime_type_set = 1;
    
    return NULL;
}

static const char *enable_xattr_handler(__unused cmd_parms *parms, void *mconfig, int flag) {
    struct mime_xattr_dir_config *c = mconfig;
    
    c->enable_handler = flag;
    c->enable_handler_set = 1;
    
    return NULL;
}

static char* get_xattr(apr_pool_t *p, const char *fn, const char *attr) {
    char v[256];
    ssize_t l;
    
    if ((l = lgetxattr(fn, attr, v, sizeof(v)-1)) <= 0)
        if ((l = getxattr(fn, attr, v, sizeof(v)-1)) <= 0)
            return NULL;

    v[l] = 0;
    
    return apr_pstrdup(p, v);
}

static char* validate_charset(char *f) {
    char *c;

    for (c = f; *c; c++) {
        *c = tolower(*c);
        if (!(*c >= 'a' && *c <= 'z') &&
            !(*c >= '0' && *c <= '9') &&
            *c != '-')
            return NULL;
    }

    return c > f ? f : NULL;
}

#define validate_encoding(f) validate_charset(f)
#define validate_handler(f) validate_charset(f)

static char* validate_mime_type(char *f) {
    char *c, *slash = NULL;
    
    for (c = f; *c; c++) {
        *c = tolower(*c);

        if (*c == '/') {
            if (!slash && c > f) {
                slash = c;
                continue;
            } else
                return NULL;
        }
            
        if (!(*c >= 'a' && *c <= 'z') &&
            !(*c >= '0' && *c <= '9') &&
            *c != '-')
            return NULL;
    }

    return c > f && slash && slash < c-1 ? f : NULL;
}

static int find_ct(request_rec *r) {
    int result = DECLINED;
    struct mime_xattr_dir_config* c;

    if (!(r->finfo.valid & APR_FINFO_TYPE) || !r->filename || r->finfo.filetype != APR_REG)
        return DECLINED;

    c = ap_get_module_config(r->per_dir_config, &mime_xattr_module);

    if (c->enable_mime_type) {
        char *mime_type, *charset, *encoding;
        
        if ((charset = get_xattr(r->pool, r->filename, XATTR_NAME_CHARSET)))
            if (!(charset = validate_charset(charset)))
                ap_log_error(APLOG_MARK, APLOG_WARNING, 0, r->server, "Bad charset specification on file '%s'", r->filename);
        
        if (!(mime_type = get_xattr(r->pool, r->filename, XATTR_NAME_MIMETYPE)))
            mime_type = get_xattr(r->pool, r->filename, XATTR_NAME_MIMETYPE2);

        if (mime_type) {
            if (!(mime_type = validate_mime_type(mime_type)))
                ap_log_error(APLOG_MARK, APLOG_WARNING, 0, r->server, "Bad MIME type specification on file '%s'", r->filename);
            else {
                if (charset)
                    mime_type = apr_psprintf(r->pool, "%s; charset=%s", mime_type, charset);
                
                r->content_type = mime_type;
                result = OK;
            }
        }

        if (charset && !mime_type && r->content_type) {
            char *a, *ct = apr_pstrdup(r->pool, r->content_type);
            static const char spec[] = "; charset=";
            
            if ((a = strstr(ct, spec))) {
                char *e;
                e = a+strlen(spec);
                e += strcspn(e, "; ");
                
                *a = 0;
                
                r->content_type = apr_psprintf(r->pool, "%s; charset=%s%s", ct, charset, e);
            } else
                r->content_type = apr_psprintf(r->pool, "%s; charset=%s", ct, charset);
        }
        
        if (!(encoding = get_xattr(r->pool, r->filename, XATTR_NAME_ENCODING)))
            encoding = get_xattr(r->pool, r->filename, XATTR_NAME_ENCODING2);

        if (encoding) {
            if (!(encoding = validate_encoding(encoding)))
                ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server, "Bad encoding specification on file '%s'", r->filename);
            else {
                r->content_encoding = encoding;
                result = OK;
            }
        }
    }

    if (c->enable_handler) {
        char *handler;

        if (!(handler = get_xattr(r->pool, r->filename, XATTR_NAME_HANDLER)))
            handler = get_xattr(r->pool, r->filename, XATTR_NAME_HANDLER2);

        if (handler) {
            if (!(handler = validate_handler(handler)))
                ap_log_error(APLOG_MARK, APLOG_ERR, 0, r->server, "Bad apache handler specification on file <%s>", r->filename);
            else {
                r->handler = handler;
                result = OK;
            }
        }
    }
    
    return result;
}

static void register_hooks(__unused apr_pool_t *p){
    static const char * const pre[] = {
        "mod_rewrite.c",
        NULL
    };

    static const char * const post[] = {
        "mod_mime.c",
        "mod_mime_magic.c",
        NULL
    };

    ap_hook_type_checker(find_ct, pre, post, APR_HOOK_MIDDLE);
}

static const command_rec commands[] = {
    
    AP_INIT_FLAG(
        "XAttrMimeType",
        enable_xattr_mime_type,
        NULL,
        OR_FILEINFO,
        "Takes 'On' or 'Off' for enabling resp. disabling usage of file system extended attribute data for MIME type detection."), 

    AP_INIT_FLAG(
        "XAttrHandler",
        enable_xattr_handler,
        NULL,
        OR_FILEINFO,
        "Takes 'On' or 'Off' for enabling resp. disabling usage of file system extended attribute data for handler detection."), 

    { NULL }
};

module AP_MODULE_DECLARE_DATA mime_xattr_module = {
    STANDARD20_MODULE_STUFF, 
    create_mime_xattr_dir_config, /* create per-dir    config structures */
    merge_mime_xattr_dir_config,  /* merge  per-dir    config structures */
    NULL,                         /* create per-server config structures */
    NULL,                         /* merge  per-server config structures */
    commands,                     /* table of config file commands */
    register_hooks                /* register hooks */
};
